package com.deepak.fitnessTracker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.w3c.dom.Text;


public class UserActivity extends AppCompatActivity {

    //private FirebaseUser user;
    //private FirebaseAuth mAuth;
    //private FirebaseAuth.AuthStateListener mAuthListener;

    private TextView name;
    private TextView distance;
    private TextView units;
    private TextView steps;
    private TextView totalSteps;
    private Button start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);


        MySqliteHelper myDb = new MySqliteHelper(this);
        steps = (TextView)findViewById(R.id.today_steps);
        totalSteps = (TextView)findViewById(R.id.total_steps);
        name = (TextView)findViewById(R.id.name);
        //get extras from prev intent
        SharedPreferences sp=getSharedPreferences("key", Context.MODE_PRIVATE);
        String email = sp.getString("email", "");
        int todaySteps = sp.getInt("today_steps", 0);
        steps.setText("Last Session : "+todaySteps);
        Users user = myDb.getUser(email);
        name.setText("Welcome "+ user.getName());


        //set ditance walked so far
        distance = (TextView)findViewById(R.id.distance);
        distance.setText(String.format("%.1f",user.getDistance()));
        units = (TextView)findViewById(R.id.units);
        units.setText("Miles");

        //set listener to start activity
        start = (Button)findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActvityTracker();
            }
        });
    }

    private void startActvityTracker() {
        Intent trackerIntent = new Intent(UserActivity.this, ActivityTracker.class);
        startActivity(trackerIntent);
        //finish();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_menu, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_reset) {
            AlertDialog alertbox = new AlertDialog.Builder(UserActivity.this)
                    .setMessage("Are you sure about reseting your counter?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                           updateValuesAndUI();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        // do something when the button is clicked
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    })
                    .show();
        }

        if(id == R.id.action_about){

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("FitnessApp\nCreated by Deepak Kaku.\nFor, The Human Project Inc")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();

                        }

                    });

            AlertDialog alert = builder.create();
            alert.show();
        }

        if(id == R.id.action_signOut){
          //  mAuth.signOut();
            SharedPreferences sp=getSharedPreferences("key", Context.MODE_PRIVATE);
            SharedPreferences.Editor ed=sp.edit();
            ed.putInt("value", 0);
            ed.putString("email", "");
            ed.commit();
            Intent userIntent = new Intent(UserActivity.this, MainActivity.class);
            //userIntent.putExtra("email", email);
            startActivity(userIntent);
            finish();
           // finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateValuesAndUI() {
        MySqliteHelper myDb = new MySqliteHelper(this);
        SharedPreferences sp=getSharedPreferences("key", Context.MODE_PRIVATE);
        String email = sp.getString("email", "");
        //int tod_step = sp.getInt("today_steps", 0);
        Users user = myDb.getUser(email);
        user.setTotal_steps(0);
        user.setDistance(0);
        myDb.updateUser(user);
        Toast.makeText(UserActivity.this, "Your counters have been reset",
                Toast.LENGTH_SHORT).show();
        steps.setText("Last Session : 0");
        totalSteps.setText("Total Session : 0");
        SharedPreferences.Editor ed=sp.edit();
        ed.putInt("today_steps", 0);
        ed.commit();
    }

    @Override
    protected void onResume(){
        super.onResume();
        MySqliteHelper myDb = new MySqliteHelper(this);
        SharedPreferences sp=getSharedPreferences("key", Context.MODE_PRIVATE);
        String email = sp.getString("email", "");
        int tod_step = sp.getInt("today_steps", 0);
        Users user = myDb.getUser(email);
        distance.setText(String.format("%.1f",user.getDistance()));
        steps.setText(String.valueOf("Last Session : "+tod_step));
        totalSteps.setText("Total Session : "+String.valueOf(user.getTotal_steps()));

        checkDistance(user.getDistance());
    }

    private void checkDistance(double distance) {
        double dist_feet = distance * 5280;
        if (dist_feet >=1000 && dist_feet <1200) {
            showFeedback(dist_feet);
        }
        else if(dist_feet >=2000 && dist_feet <2200){
            showFeedback(dist_feet);
        }


    }

    private void showFeedback(double dist_feet){
        AlertDialog alertbox = new AlertDialog.Builder(UserActivity.this)
                .setMessage("Congratulations! you achieved a new milestone.\nYou completed"+dist_feet+" feet.\nKeep up the good work")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                })

                .show();
    }


}
