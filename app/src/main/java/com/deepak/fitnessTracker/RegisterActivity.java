package com.deepak.fitnessTracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private EditText mName;
    private EditText mUsername;
    private EditText mPassword;
    private EditText mPassword2;
    private EditText mAge;
    private EditText mWeight;
    private EditText mHeight;
    private int heightFt;
    private int heightIn;
    private RadioGroup mGender;
    private RadioButton gender;
    private Button mRegister;
    //SQLiteDatabase myDb = openOrCreateDatabase("fitnessApp.db", MODE_PRIVATE, null);
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                    // User is signed in
                    //Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    //Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };


        mName = (EditText)findViewById(R.id.name);
        mUsername = (EditText)findViewById(R.id.username);
        mPassword = (EditText)findViewById(R.id.password);
        mPassword2 = (EditText)findViewById(R.id.password2);
        mAge = (EditText)findViewById(R.id.etAge);
        mHeight = (EditText)findViewById(R.id.height);
        mWeight = (EditText)findViewById(R.id.weight);
        mGender = (RadioGroup)findViewById(R.id.gender);
        mRegister = (Button)findViewById(R.id.register);
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAccount(mUsername.getText().toString(), mPassword.getText().toString());
                //user = mAuth.getCurrentUser();
                /*UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                        .setDisplayName(mName.getText().toString()).build();
                user.updateProfile(profileUpdates);
                */
                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    private void createAccount(String email, String password) {

        if (!validateForm()) {
            return;
        }
    /*
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (!task.isSuccessful()) {
                            Toast.makeText(RegisterActivity.this, "Username already exists",
                                    Toast.LENGTH_SHORT).show();

                        }

                        else{
                            Toast.makeText(RegisterActivity.this, "Registered Successfully! Please Login",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        */

        //addValuesToDb();
        finishRegistration();
    }

    private void finishRegistration() {
      //  user = mAuth.getCurrentUser();
        //if(user==null)
          //  System.out.println("NULL AAYA YAAR");
        MySqliteHelper mySqliteHelper = new MySqliteHelper(this);
        int genId = mGender.getCheckedRadioButtonId();
        gender = (RadioButton) findViewById(genId);
        //System.out.println(gender.getText().toString());
        //addValuesToDb();
        //add the user to local database
        Users userIn = new Users();
        //userIn.setUid(0);
        userIn.setName(mName.getText().toString());
        userIn.setEmail(mUsername.getText().toString());
        userIn.setAge(Integer.parseInt(mAge.getText().toString()));
        userIn.setGender(gender.getText().toString());
        String hashedPass = generateHashedPAssword(mPassword.getText().toString());
        userIn.setPassword(hashedPass);
        userIn.setWeight(Double.parseDouble(mWeight.getText().toString()));
        String ht =  mHeight.getText().toString();
        String[] htSplit = ht.split("\\.");
        userIn.setHeightFt(Integer.parseInt(htSplit[0]));
        userIn.setHeightIn(Integer.parseInt(htSplit[1]));
        userIn.setDistance(0);
        mySqliteHelper.addUsers(userIn);
        Toast.makeText(RegisterActivity.this, "Registered Successfully! Please Login",
                Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    private String generateHashedPAssword(String ogPass) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(ogPass.getBytes());
        byte byteData[] = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }


    private void addValuesToDb() {
       // myDb.execSQL("INSERT into userActivity VALUES ('"+user.getDisplayName()+"','"+user.getEmail()+"',"+mAge+",'");
    }

    private boolean validateForm() {
        boolean valid = true;

        String name = mName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            mName.setError("Required");
            valid = false;
        } else {
            mName.setError(null);
        }

        String username = mUsername.getText().toString();
        if (TextUtils.isEmpty(username)) {
            mUsername.setError("Required.");
            valid = false;
        } else {
            mUsername.setError(null);
        }

        String password = mPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPassword.setError("Required");
            valid = false;
        }
        else {
            mPassword.setError(null);
        }

        String password2 = mPassword2.getText().toString();
        if (TextUtils.isEmpty(password2)) {
            mPassword2.setError("Required");
            valid = false;
        }
        else if(!(password2.equals(password))){
            mPassword2.setError("Passwords Don't match");
            valid = false;
        }
        else {
            mPassword2.setError(null);
        }

        String age = mAge.getText().toString();
        if(TextUtils.isEmpty(age)){
            mAge.setError("Required");
            valid=false;
        }
        else{
            mAge.setError(null);
        }

        int rgId = mGender.getCheckedRadioButtonId();
        if(rgId == -1){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Please select your Gender!")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();

                        }

                    });

            AlertDialog alert = builder.create();
            alert.show();
            valid = false;
        }

        return valid;
    }
}
