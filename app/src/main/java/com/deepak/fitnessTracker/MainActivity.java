package com.deepak.fitnessTracker;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.provider.*;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    //private FirebaseAuth mAuth;
    //private FirebaseAuth.AuthStateListener mAuthListener;
    //private GoogleApiClient mGoogleApiClient;
    private EditText mEmailField;
    private EditText mPasswordField;
    private Button mLogin;
    private TextView mRegister;
    //private FirebaseUser user;
    //SQLiteDatabase myDb = openOrCreateDatabase("fitnessApp.db", MODE_PRIVATE, null);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sp=getSharedPreferences("key", Context.MODE_PRIVATE);
        int value = sp.getInt("value", 0);
        String email = sp.getString("email", "");

        if(value==1){
            Intent userIntent = new Intent(MainActivity.this, UserActivity.class);
            //userIntent.putExtra("email", email);
            startActivity(userIntent);
            finish();
        }
        /*
        user = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();

        //myDb.execSQL("CREATE TABLE IF NOT EXISTS userActivity(name VARCHAR, email VARCHAR, age INT, gender VARCHAR,weight INT, heightFt INT, heightIn INT, distance INT)");

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                    // User is signed in
                    //Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {

                    // User is signed out
                    //Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        */
        mLogin = (Button)findViewById(R.id.login);
        mRegister = (TextView) findViewById(R.id.register);
        mEmailField = (EditText)findViewById(R.id.username);
        mPasswordField = (EditText)findViewById(R.id.password);

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
            }
        });

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //createAccount(mEmailField.getText().toString(), mPasswordField.getText().toString());
                Intent registerIntent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
                finish();
            }
        });

    }

    private void signIn(String email, String password) {

        if (!validateForm()) {
            return;
        }

        String hashedPass = generateHashedPAssword(password);

        MySqliteHelper myDb = new MySqliteHelper(this);
        Users returnedUser = myDb.getUser(email);
        if(returnedUser!=null) {
            if (hashedPass.equals(returnedUser.getPassword())) {
                Toast.makeText(MainActivity.this, "Login Success ",
                        Toast.LENGTH_SHORT).show();
                SharedPreferences sp=getSharedPreferences("key", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed=sp.edit();
                ed.putInt("value", 1);
                ed.putString("email", returnedUser.getEmail());
                ed.commit();
                Intent userIntent = new Intent(MainActivity.this, UserActivity.class);
                //userIntent.putExtra("email", email);
                startActivity(userIntent);
                finish();
            }
            else{
                Toast.makeText(MainActivity.this, R.string.auth_failed,
                        Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(MainActivity.this, "User Does not exists",
                    Toast.LENGTH_SHORT).show();
        }

        //System.out.println(returnedUser.getPassword());
        /*
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (!task.isSuccessful()) {

                            Toast.makeText(MainActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{
                            user = mAuth.getCurrentUser();
                            Toast.makeText(MainActivity.this, "Login Success "+user.getUid(),
                                    Toast.LENGTH_SHORT).show();
                            Intent userIntent = new Intent(MainActivity.this, UserActivity.class);
                            //userIntent.putExtra("Name",user.getDisplayName());
                            startActivity(userIntent);
                            finish();
                        }

                    }
                });
                */
        // [END sign_in_with_email]
    }

    private String generateHashedPAssword(String ogPass) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(ogPass.getBytes());
        byte byteData[] = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }


    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }

    private void updateUI(FirebaseUser user) {
      //  hideProgressDialog();
        if (user != null) {
            /*mStatusTextView.setText(getString(R.string.emailpassword_status_fmt, user.getEmail()));
            mDetailTextView.setText(getString(R.string.firebase_status_fmt, user.getUid()));

            findViewById(R.id.email_password_buttons).setVisibility(View.GONE);
            findViewById(R.id.email_password_fields).setVisibility(View.GONE);
            findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);
            */
        } else {
           /* mStatusTextView.setText(R.string.signed_out);
            mDetailTextView.setText(null);

            findViewById(R.id.email_password_buttons).setVisibility(View.VISIBLE);
            findViewById(R.id.email_password_fields).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_button).setVisibility(View.GONE);
            */
        }
    }

    @Override
    public void onClick(View v) {

    }


}
