package com.deepak.fitnessTracker;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.IntegerRes;
import android.support.v4.app.INotificationSideChannel;

/**
 * Created by Deepak Kaku on 08-01-2017.
 */

public class MySqliteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "fitnessAppUsers";
    private static final String TABLE_USERS = "users";
    private static final String KEY_ID = "uid";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_AGE = "age";
    private static final String KEY_GENDER = "gender";
    private static final String KEY_WEIGHT = "weight";
    private static final String KEY_HEIGHTFT = "heightFt";
    private static final String KEY_HEIGHTIN = "heightIn";
    private static final String KEY_DISTANCE = "distance";
    private static final String KEY_STEPS = "total_steps";
    private String[] USERS_COLUMNS = {KEY_ID, KEY_NAME, KEY_EMAIL, KEY_PASSWORD, KEY_AGE, KEY_GENDER, KEY_WEIGHT, KEY_HEIGHTFT, KEY_HEIGHTIN, KEY_DISTANCE, KEY_STEPS};
    public MySqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS "+TABLE_USERS+"("+
                KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+KEY_NAME+" VARCHAR,"+KEY_EMAIL+" VARCHAR,"+KEY_PASSWORD+" VARCHAR,"+KEY_AGE+" INTEGER,"+
                KEY_GENDER+" VARCHAR,"+KEY_WEIGHT+" INTEGER,"+KEY_HEIGHTFT+" INTEGER,"+KEY_HEIGHTIN+" INTGER,"+
                KEY_DISTANCE+" INTEGER,"+KEY_STEPS+" INTEGER);";
        System.out.println(CREATE_USERS_TABLE);
        sqLiteDatabase.execSQL(CREATE_USERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_USERS);
        onCreate(sqLiteDatabase);
    }

    public void addUsers(Users users){
        SQLiteDatabase myWriteDb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //contentValues.put(KEY_ID, users.getUid());
        contentValues.put(KEY_NAME, users.getName());
        contentValues.put(KEY_EMAIL, users.getEmail());
        contentValues.put(KEY_PASSWORD, users.getPassword());
        contentValues.put(KEY_AGE, users.getAge());
        contentValues.put(KEY_GENDER, users.getGender());
        contentValues.put(KEY_WEIGHT, users.getWeight());
        contentValues.put(KEY_HEIGHTFT, users.getHeightFt());
        contentValues.put(KEY_HEIGHTIN, users.getHeightIn());
        contentValues.put(KEY_DISTANCE, users.getDistance());
        contentValues.put(KEY_STEPS, users.getTotal_steps());

        myWriteDb.insert(TABLE_USERS, null, contentValues);
        myWriteDb.close();
    }

    public Users getUser(String email) {

        SQLiteDatabase myReadDb = this.getReadableDatabase();
        //Cursor cursor = myReadDb.query(TABLE_USERS, USERS_COLUMNS, "email=?", new String[]{String.valueOf(email)}, null, null, null, null);
        Cursor cursor = myReadDb.rawQuery("SELECT * from " + TABLE_USERS
                + " WHERE email='"+email+"';" , null);
        Users users = null;
        if (cursor != null && cursor.moveToFirst()) {

        int uid = cursor.getColumnIndex(KEY_ID);
        String name = cursor.getString(1);
        String email2 = cursor.getString(2);
        String password = cursor.getString(3);
        int age = Integer.parseInt(cursor.getString(4));
        String gender = cursor.getString(5);
        double weight = Double.parseDouble(cursor.getString(6));
        int heightFt = Integer.parseInt(cursor.getString(7));
        int heightIn = Integer.parseInt(cursor.getString(8));
        double distance = Double.parseDouble(cursor.getString(9));
        int total_steps = Integer.parseInt(cursor.getString(10));

        users = new Users(uid, name, email2, password, age, gender, weight, heightFt, heightIn, distance,total_steps );
        }
        else{
            return null;
        }
        return users;
    }

    public boolean updateUser(Users users) {
        SQLiteDatabase myWriteDb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, users.getName());
        contentValues.put(KEY_EMAIL, users.getEmail());
        contentValues.put(KEY_PASSWORD, users.getPassword());
        contentValues.put(KEY_AGE, users.getAge());
        contentValues.put(KEY_GENDER, users.getGender());
        contentValues.put(KEY_WEIGHT, users.getWeight());
        contentValues.put(KEY_HEIGHTFT, users.getHeightFt());
        contentValues.put(KEY_HEIGHTIN, users.getHeightIn());
        contentValues.put(KEY_DISTANCE, users.getDistance());
        contentValues.put(KEY_STEPS, users.getTotal_steps());

        myWriteDb.update(TABLE_USERS, contentValues, "email =?", new String[]{users.getEmail()});
        myWriteDb.close();
        return true;
    }
}
