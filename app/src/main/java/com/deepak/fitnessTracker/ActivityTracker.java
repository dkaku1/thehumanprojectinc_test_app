package com.deepak.fitnessTracker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import static android.support.v7.appcompat.R.styleable.View;

public class ActivityTracker extends AppCompatActivity implements SensorEventListener{

    private TextView timer;
    private TextView steps;
    private ImageView pause;
    private Button stop;

    private Timer T;
    private int seconds=0;
    private int minutes=0;
    private int hours = 0;
    private int total_steps = 0;
    private int value = 0;
    private int countDown=60;
    private boolean isPause = false;
    private Vibrator vibrator;

    //sensor variables
    private SensorManager mSensorManager;
    private Sensor mStepCounterSensor;
    private Sensor mStepDetectorSensor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);
        getSupportActionBar().hide();

        //set up the sensor manager
        mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        mStepCounterSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        mStepDetectorSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        //access DB
        MySqliteHelper mySqliteHelper = new MySqliteHelper(this);
        SharedPreferences sp=getSharedPreferences("key", Context.MODE_PRIVATE);
        String email = sp.getString("email", "");
        total_steps = sp.getInt("today_steps", 0);
        Users user = mySqliteHelper.getUser(email);
        int heightFt = user.getHeightFt();
        int heightIn = user.getHeightIn();
        if(heightFt==0 || heightIn==0){
            AlertDialog alertbox = new AlertDialog.Builder(this)
                    .setMessage("You have not mentioned your height in settings. Height help us calculate accurate distance traveled\n" +
                            "Do you want to continue?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            //removeListeners();

                            //finish();
                            //close();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        // do something when the button is clicked
                        public void onClick(DialogInterface arg0, int arg1) {
                            removeListeners();

                            finish();
                        }
                    })
                    .show();
        }

        timer = (TextView)findViewById(R.id.timer);
        steps = (TextView)findViewById(R.id.steps);
        stop = (Button)findViewById(R.id.stop_button);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alertbox = new AlertDialog.Builder(ActivityTracker.this)
                        .setMessage("Do you want to exit Tracker?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                removeListeners();
                                updateValuesinDB();
                                finish();
                                //close();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            // do something when the button is clicked
                            public void onClick(DialogInterface arg0, int arg1) {
                            }
                        })
                        .show();

            }
        });
        pause = (ImageView)findViewById(R.id.pause_icon);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isPause){
                    resumeTimer();
                }
                else {
                    pauseTimer();
                }
            }
        });
        startTimer();
    }

    private void updateValuesinDB() {
        MySqliteHelper mySqliteHelper = new MySqliteHelper(this);
        SharedPreferences sp=getSharedPreferences("key", Context.MODE_PRIVATE);
        String email = sp.getString("email", "");
        Users user = mySqliteHelper.getUser(email);
        int heightFt = user.getHeightFt();
        int heightIn = user.getHeightIn();
        double miles;
        if(heightFt==0 || heightIn==0){
            int heightInMeters = (int) ((5*0.3048) + (5*0.0254));
            miles = (0.45*heightInMeters*Integer.parseInt(steps.getText().toString()))*0.000621;
            System.out.println(miles);
        }
        else{
            int heightInMeters = (int) ((heightFt*0.3048) + (heightIn*0.0254));
            miles = (0.45*heightInMeters*Integer.parseInt(steps.getText().toString()))*0.000621;
            System.out.println(miles);
        }
        user.setDistance(miles);
        int todaySteps = Integer.parseInt(steps.getText().toString());
        user.setTotal_steps(user.getTotal_steps()+todaySteps);
        mySqliteHelper.updateUser(user);

        //write steps to Shared prefs
        SharedPreferences sp1=getSharedPreferences("key", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed=sp1.edit();
        ed.putInt("today_steps", todaySteps);
        ed.commit();
    }

    private void resumeTimer() {
        isPause = false;
        pause.setBackgroundResource(R.drawable.ic_pause_white_48dp);
        mSensorManager.registerListener(this, mStepCounterSensor, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mStepDetectorSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    private void pauseTimer() {
        isPause = true;
        pause.setBackgroundResource(R.drawable.ic_play_white_48dp);
    }

    private void startTimer() {
        T=new Timer();
        T.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        timer.setText(String.format("%02d",hours)+":"+String.format("%02d",minutes)+":"+String.format("%02d",seconds));
                        if(!isPause) {
                            seconds += 1;
                        }

                        if(seconds==60){
                            minutes += 1;
                            countDown--;
                            seconds=0;
                        }
                        if(minutes==60){
                            hours +=1;
                            minutes =0;
                        }

                        if(countDown==0){
                            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(500);
                            AlertDialog alertbox = new AlertDialog.Builder(ActivityTracker.this)
                                    .setMessage("WAKE UP!! Start walking")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface arg0, int arg1) {

                                        }
                                    })
                                    .show();
                        }

                        if(isPause){
                            removeListeners();
                        }

                    }
                });
            }
        }, 1000, 1000);




    }

    @Override
    public void onBackPressed(){
        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to exit Tracker?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        removeListeners();
                        updateValuesinDB();
                        finish();
                        //close();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();
    }

    private void removeListeners() {
        mSensorManager.unregisterListener(this, mStepCounterSensor);
        mSensorManager.unregisterListener(this, mStepDetectorSensor);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor sensor = sensorEvent.sensor;
        //float[] sensorValues = sensorEvent.values;
        countDown = 60;
        if(sensor.getType()==Sensor.TYPE_STEP_COUNTER){
            steps.setText(String.valueOf(value));
            value++;

        }
        else if(sensor.getType()==Sensor.TYPE_STEP_DETECTOR){
            System.out.println(value);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume(){
        super.onResume();
        mSensorManager.registerListener(this, mStepCounterSensor, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mStepDetectorSensor, SensorManager.SENSOR_DELAY_FASTEST);

    }


}
